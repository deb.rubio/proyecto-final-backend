# -*- coding: utf-8 -*-
# =========================
# Librarys
# =========================
import os
from os.path import join
from flask import Flask
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from dotenv import load_dotenv, find_dotenv
# para generar contraseñas falsas y encriptada
from werkzeug.security import generate_password_hash
from faker import Factory
from random import randint

# aqui automáticamente le decimos que coja el entorno virtual .venv 
# y tenemos que importar para que funcione el find_dotenv from dotenv
# y borramos el dirname importado
load_dotenv(find_dotenv())


app = Flask(__name__)

# =========================
# Settings
# =========================
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URI')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# =========================
# Variables
# =========================
db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)



class User(db.Model):
    '''
    Table user
    '''

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    username = db.Column(db.String(100))
    mail = db.Column(db.String(200))
    password = db.Column(db.String(106))
    created_at = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<User Table {0}>'.format(self.username)


class Category(db.Model):
    '''
    Table Category
    '''

    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return '<Category Table {0}>'.format(self.name)


class Article(db.Model):
    '''
    Table user
    '''

    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(100))
    created_at = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)
    resumen = db.Column(db.String(300))
    texto = db.Column(db.Text)
    categoria_id = db.Column(
        db.Integer, db.ForeignKey('categories.id'), nullable=False)
    autor_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), nullable=False)
    visitas = db.Column(db.Integer)
    imagen = db.Column(db.String(1000))

    # Relations
    user = db.relationship('User', backref=db.backref(
        'Article', lazy=True, cascade="all, delete-orphan"))
    category = db.relationship('Category', backref=db.backref(
        'Article', lazy=True, cascade="all"))

    def __repr__(self):
        return '<Article Table {0}>'.format(self.titulo)


class Comment(db.Model):
    '''
    Table Comment
    '''

    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100))
    texto = db.Column(db.Text)
    article_id = db.Column(
        db.Integer, db.ForeignKey('articles.id'), nullable=False)
    created_at = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)

    # Relations
    article = db.relationship('Article', backref=db.backref(
        'Comment', lazy=True, cascade="all, delete-orphan"))

    def __repr__(self):
        return '<Comment Table {0}>'.format(self.id)

class Newsletter(db.Model):
    '''
    Table Newsletter
    '''

    __tablename__ = 'newsletters'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255))
    created_at = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<Newsletter Table {0}>'.format(self.email)


# =========================
# Help commands
# =========================

@manager.command
def init_data():

    # Reload tables
    db.drop_all()
    db.create_all()

    # Make 2 categories
    my_categories = ('Articulos', 'Recetas')
    for category in my_categories:
        my_new_category = Category()
        my_new_category.name = category
        db.session.add(my_new_category)

    # Make new user (añadiendo nosotros los datos)
    # creamos la variable y llamamos a la tabla que tendrá los datos
    user_admin = User()
    user_admin.name = 'Debora Rubio'
    user_admin.username = 'deruso'
    user_admin.mail = 'debora.rubio.soliva@gmail.com'
    user_admin.password = generate_password_hash('123')
    # añadimos
    db.session.add(user_admin)
    # comiteamos
    db.session.commit()

    print('Base de datos lista')

@manager.command
def fake_data():
    # Spanish: genera el objeto para la info falsa
    fake = Factory.create('es_ES')

    # Reload tables
    db.drop_all()
    db.create_all()

    # Make 3 fake user: vamos a https://faker.readthedocs.io/en/master/locales/es_ES.html para copiar funcionalidades
    for num in range(3):
        profile = fake.simple_profile()
        name = fake.name()
        username = profile['username']
        mail = profile['mail']
        password = generate_password_hash('123')
        # Save in database
        my_user = User(name=name, username=username, mail=mail, password=password)
        db.session.add(my_user)

    print('Users created')

    # Make 2 categories
    my_categories = ('Articulos', 'Recetas')
    for category in my_categories:
        my_new_category = Category()
        my_new_category.name = category
        db.session.add(my_new_category)

    print('Categories created')

    # Make 10 fake comments
    for num in range(10):
        texto = fake.text()
        article_id = randint(1, 100)
        username = fake.user_name()
        # Save in database
        my_comment = Comment(texto=texto, article_id=article_id, username=username)
        db.session.add(my_comment)

    print('Comments created')

    # Make 20 fake articles
    for num in range(14):
        titulo = fake.sentence(nb_words=4)
        resumen = fake.paragraph(nb_sentences=1)
        texto = fake.text(max_nb_chars=1500)
        categoria_id = randint(1, 3)
        autor_id = randint(1, 100)
        visitas = randint(1, 1000)
        imagen = '/static/img/img.jpg'
        # podemos poner una url entre las '' tambien 
        # imagen = fake.image_url()
        # Save in database
        my_article = Article(titulo=titulo, resumen=resumen, texto=texto, categoria_id=categoria_id, autor_id=autor_id, visitas=visitas, imagen=imagen)
        db.session.add(my_article)

    print('Articles created')

    db.session.commit()


if __name__ == "__main__":
    manager.run()