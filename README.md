# proyecto-final-backend

## Install

```bash
virtualenv --python=python3 .venv
source .venv/bin/activate
pip3 install -r requirements.txt
python3 models.py db init
python3 models.py db migrate
python3 models.py db upgrade
python3 models.py init_data
```

## Run

```bash
source .venv/bin/activate
python3 app.py
```

## Tools

Generar infomación falsa para el desarrollo

```bash
python3 models.py fake_data
```