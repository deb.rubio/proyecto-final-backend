# -*- coding: utf-8 -*-

# =========================
# Librarys
# =========================
import os
import requests
from functools import wraps
from os.path import join, dirname
from dotenv import load_dotenv, find_dotenv
from flask import Flask, request, render_template, session, redirect, url_for
from flask_restplus import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from models import User, Article, Category, Comment, Newsletter
from flask_marshmallow import Marshmallow
from flask_cors import CORS

# =========================
# Extensions initialization
# =========================

# aqui automáticamente le decimos que coja el entorno virtual .venv 
# y tenemos que importar para que funcione el find_dotenv from dotenv
# y borramos el dirname importado
load_dotenv(find_dotenv())

# (lo que hemos borrado ha sido esto):
    ## dotenv_path = join(dirname(__file__), 'env')
    ## load_dotenv(dotenv_path)

app = Flask(__name__)

# =========================
# Variables
# =========================

app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY')
app.config['DEBUG'] = True if os.environ.get('DEBUG') == 'True' else False
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URI')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

MAX_ITEMS = 5
MAX_SEARCH = 5
# Como solo es un user, guardamos el email y la contraseña aqui (si fuesen más, lo guardaríamos en la BBDD y daríamos roles)
ADMIN_EMAIL = os.environ.get('ADMIN_EMAIL')
ADMIN_PASSWORD = os.environ.get('ADMIN_PASSWORD')


PRE_URL = '/api/v1/'
CORS(app)
ma = Marshmallow(app)
api = Api(app)
db = SQLAlchemy(app)
db.init_app(app)

# DECORATIONS


def login_required(f):
    # Decoration: check login in session
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user' not in session:
            session.clear()
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated_function

# END DECORATIONS

# =========================
# Schemas
# =========================


# User
class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose (del usuario solo necesitamos el id, nombre y el username)
        fields = ('id', 'name', 'username')


## esquemas que usaremos en las rutas!!
user_schema = UserSchema()
users_schema = UserSchema(many=True)

# Category
class CategorySchema(ma.Schema):
    class Meta:
        # Fields to expose (de la categoría solo necesitamos el id y el nombre)
        fields = ('id', 'name')


category_schema = CategorySchema()
categories_schema = CategorySchema(many=True)

# Comment
class CommentSchema(ma.Schema):
    class Meta:
        # Fields to expose 
        fields = ('id', 'username', 'texto', 'created_at', '_links')

    #Creamos un link para que nos de el id de cada articulo que vamos a necesitar para ubicar los comentarios
    _links = ma.Hyperlinks({
        'article': ma.URLFor('single_article', id='<article_id>')
    })


comment_schema = CommentSchema()
comments_schema = CommentSchema(many=True)

# Article
class ArticleSchema(ma.Schema):
    class Meta:
        # Fields to expose (de la categoría solo necesitamos el id y el nombre)
        fields = ('id', 'titulo', 'resumen', 'texto', 'imagen', '_links', 'categoria_id', 'created_at')

    _links = ma.Hyperlinks({
        'category': ma.URLFor('single_category', id='<categoria_id>'),
        'autor': ma.URLFor('single_user', id='<autor_id>')
    })


article_schema = ArticleSchema()
articles_schema = ArticleSchema(many=True)

# Newsletter
class NewsletterSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('id', 'email')


## esquemas que usaremos en las rutas!!
newsletter_schema = NewsletterSchema()
newsletters_schema = NewsletterSchema(many=True)



# =========================
# Routes
# =========================



# User

@api.route(PRE_URL + 'user/<int:id>')
class SingleUser(Resource):

    def get(self, id):
        #User (nombre de la tabla)
        my_user = User.query.get(id)
        if my_user:
            return user_schema.jsonify(my_user)
        else:
            return {'message': 'No existe el usuario'}, 400

# Category

@api.route(PRE_URL + 'category/<int:id>')
class SingleCategory(Resource):

    def get(self, id):
        #Category (nombre de la tabla)
        my_category = Category.query.get(id)
        if my_category:
            return category_schema.jsonify(my_category)
        else:
            return {'message': 'No existe la categoria'}, 400

# Comment

@api.route(PRE_URL + 'article/<int:article_id>/comment')
class CommentList(Resource):

    def get(self, article_id):
        #Comment (nombre de la tabla)
        my_commentaries = Comment.query.filter_by(article_id=article_id).all()
        if my_commentaries:
                    #el nombre del esquema lo tenemos definicdo arriba, tanto para 1 resultado como para todos
            return comments_schema.jsonify(my_commentaries)
        else:
            return {'message': 'No existen los comentarios'}, 400

# Article

@api.route(PRE_URL + 'article/<int:id>')
class SingleArticle(Resource):

    def get(self, id):
        #Comment (nombre de la tabla)
        my_article = Article.query.get(id)
        if my_article:
                    #el nombre del esquema lo tenemos definido arriba, tanto para 1 resultado como para todos
            return article_schema.jsonify(my_article)
        else:
            return {'message': 'No existe el artículo'}, 400

# Articles

@api.route(PRE_URL + 'article')
class ArticleList(Resource):

    def get(self):
        #Comment (nombre de la tabla)
        my_article = Article.query.order_by(Article.created_at.desc()).all()
        if my_article:
                    #el nombre del esquema lo tenemos definido arriba, tanto para 1 resultado como para todos
            return articles_schema.jsonify(my_article)
        else:
            return {'message': 'No existe el artículo'}, 400

# Articles categories

@api.route(PRE_URL + 'article/category/<int:category_id>/pag/<int:pag>')
class ArticleCategoryPag(Resource):

    def get(self, category_id, pag):
        #Calculamos paginador
        start = ((pag - 1) * MAX_ITEMS)
        end = pag * MAX_ITEMS
        # filtro: si la categoria es 0, los muestra todos (ordenados por creación descendente)
        if category_id == 0:
            my_article = Article.query.order_by(Article.created_at.desc()).slice(start, end).all()
        # filtro: si la categoria no es 0, los filtramos por categoria
        else:
            my_article = Article.query.filter_by(categoria_id=category_id).order_by(Article.created_at.desc()).slice(start, end).all()
        # por tanto, si existe, lo mostramos
        if my_article:
                    #el nombre del esquema lo tenemos definido arriba, tanto para 1 resultado como para todos
            return articles_schema.jsonify(my_article)
        else:
            return {'message': 'No existe el artículo'}, 400

@api.route(PRE_URL + 'newsletter')
class NewsletterSingle(Resource):

    def post(self):
        # recopilamos la info que me manda el client
        json_data = request.get_json()
        # guardamos en la base de datos estos datos
        my_newsletter = Newsletter()
        my_newsletter.email = json_data['email']
        db.session.add(my_newsletter)
        db.session.commit()
        return ''

@api.route(PRE_URL + 'comments')
class addComentario(Resource):

    def post(self):
        # recopilamos la info que me manda el client
        json_data = request.get_json()
        print('hola')
        # guardamos en la base de datos estos datos
        my_commentary = Comment()
        my_commentary.username = json_data['username']
        my_commentary.texto = json_data['texto']
        my_commentary.article_id = json_data['article_id']
        db.session.add(my_commentary)
        db.session.commit()
        return ''

@api.route(PRE_URL + 'newsletters')
class NewsletterList(Resource):

    def get(self):
        #Comment (nombre de la tabla)
        my_newsletter = Newsletter.query.all()
        print(my_newsletter)
        print('todo bien')
        if my_newsletter:
            return newsletters_schema.jsonify(my_newsletter)
        else:
            return {'message': 'No hay correos'}, 400

@api.route(PRE_URL + 'search/<query>/pag/<pag>')
class SearchAll(Resource):

    def get(self, query, pag):
        #Calculamos paginador
        start = ((int(pag) - 1) * MAX_SEARCH)
        end = int(pag) * MAX_SEARCH
        my_article = Article.query.order_by(Article.created_at.desc()).filter(Article.texto.ilike(f'%{query}%')).slice(start, end).all()
        if my_article:
            # OJO HAY QUE PASARSELO AL SCHEMA DE TODOS, NO ARTICLE_SCHEMA
            return articles_schema.jsonify(my_article)
        else:
            return {'message': 'No hay ningún artículo que contenga lo que buscas'}, 200

@api.route(PRE_URL + 'contact')
class SendContact(Resource):

    def post(self):
        # recopilamos la info que me manda el client
        json_data = request.get_json()
        # enviamos directamente sin pasar por base de datos
        texto_final = f'''
                  Correo de contacto de "La LTP y yo"
                  
                  Nombre: {json_data['name']}
                  Email: {json_data['email']}
                  Mensaje: {json_data['message']}
                  '''
        # Try running this locally.
        requests.post(
            "https://api.mailgun.net/v3/sandbox9a13e0babcea499a9f45214967ee19e8.mailgun.org/messages",
            auth=("api", "key-8f41b3ba6689ae11048d42d7890f596e"),
            data={"from": "La LTP y yo <no-reply@laltpyyo.com>",
                  "to": ["debora.rubio.soliva@gmail.com"],
                  "subject": "Contacto",
                  "text": texto_final})
        return {'message': 'Todo ok'}, 200

# Administración 
@app.route("/login", methods=('GET','POST'))
def login():
    # Recogemos el email y la contraseña de la persona que se está registrando
    if request.method == 'POST':
        # Recuperamos datos del formulario
        my_email = request.form['email']
        my_password = request.form['password']
        # Comprobamos si son correctos
        if my_email == ADMIN_EMAIL and my_password == ADMIN_PASSWORD:
            # Creamos una sesión
            session['user'] = ADMIN_EMAIL
            # Redireccionamos a la pantalla del dashboard
            return redirect(url_for('dashboard'))
        else: 
            #Si el correo o contraseña no son correctas (no coinciden)
            return render_template('login.html', error=True)
    return render_template('login.html')

## los que no tengan methods es porque por defecto solo tienen GET
@app.route("/dashboard")
@login_required
def dashboard():
    return render_template('dashboard.html')

@app.route("/dashboard/articulos")
@login_required
def dashboard_articulos():
    articulos = Article.query.all()
    return render_template('articulos.html', articulos=articulos)

@app.route("/dashboard/categorias")
@login_required
def dashboard_categorias():
    categorias = Category.query.all()
    return render_template('categorias.html', categorias=categorias)

@app.route("/dashboard/comentarios")
@login_required
def dashboard_comentarios():
    comentarios = Comment.query.all()
    return render_template('comentarios.html', comentarios=comentarios)

@app.route("/articulos/nuevo", methods=('GET','POST'))
@login_required
def articulo_nuevo():
    categorias = Category.query.all()
    usuarios = User.query.all()
    if request.method == 'POST':
        # Obtenemos la info del formulario de POST
        titulo = request.form['titulo']
        resumen = request.form['resumen']
        texto = request.form['texto']
        categoria = request.form['categoria']
        autor = request.form['autor']
        imagen = request.form['imagen']
        # Guardamos en la base de datos
        my_article = Article()
        my_article.titulo = titulo
        my_article.resumen = resumen
        my_article.texto = texto
        my_article.categoria_id = categoria
        my_article.autor_id = autor
        my_article.imagen = imagen

        db.session.add(my_article)
        db.session.commit()

        return redirect(url_for('dashboard_articulos'))

    return render_template('articulo_nuevo.html', categorias=categorias, usuarios=usuarios)

@app.route("/comentarios/nuevo", methods=('GET','POST'))
@login_required
def comentario_nuevo():
    articulos = Article.query.all()
    usuarios = User.query.all()
    comentarios = Comment.query.all()
    if request.method == 'POST':
        # Obtenemos la info del formulario de POST
        autor = request.form['autor']
        texto = request.form['texto']
        articulo = request.form['articulo']
        # Guardamos en la base de datos
        my_commentary = Comment()
        my_commentary.texto = texto
        my_commentary.username = autor
        my_commentary.article_id = articulo

        db.session.add(my_commentary)
        db.session.commit()

        return redirect(url_for('dashboard_comentarios'))

    return render_template('comentario_nuevo.html', articulos=articulos, usuarios=usuarios, comentarios=comentarios)

@app.route("/categorias/nuevo", methods=('GET','POST'))
@login_required
def categoria_nueva():
    categorias = Category.query.all()
    if request.method == 'POST':
        # Obtenemos la info del formulario de POST
        name = request.form['name']
        # Guardamos en la base de datos
        my_category = Category()
        my_category.name = name

        db.session.add(my_category)
        db.session.commit()

        return redirect(url_for('dashboard_categorias'))
    return render_template('categoria_nueva.html', categorias=categorias)

@app.route("/articulos/editar/<int:id>", methods=('GET','POST'))
@login_required
def articulo_editar(id):
    categorias = Category.query.all()
    usuarios = User.query.all()
    # Para que coja uno que ya existe y no cree uno nuevo
    my_article = Article.query.get(id)
    if request.method == 'POST':
        # Obtenemos la info del formulario de POST
        titulo = request.form['titulo']
        resumen = request.form['resumen']
        texto = request.form['texto']
        categoria = request.form['categoria']
        autor = request.form['autor']
        imagen = request.form['imagen']
        # Guardamos en la base de datos
        my_article.titulo = titulo
        my_article.resumen = resumen
        my_article.texto = texto
        my_article.categoria_id = categoria
        my_article.autor_id = autor
        my_article.imagen = imagen

        db.session.commit()

        return redirect(url_for('dashboard_articulos'))
    return render_template('articulo_editar.html', categorias=categorias, usuarios=usuarios, article=my_article)

@app.route("/categoria/editar/<int:id>", methods=('GET','POST'))
@login_required
def categoria_editar(id):
    # Para que coja uno que ya existe y no cree uno nuevo
    my_category = Category.query.get(id)
    if request.method == 'POST':
        # Obtenemos la info del formulario de POST
        name = request.form['name']
        # Guardamos en la base de datos
        my_category.name = name

        db.session.commit()

        return redirect(url_for('dashboard_categorias'))
    return render_template('categoria_editar.html', categoria=my_category)

@app.route("/articulos/borrar/<int:id>")
@login_required
def articulo_borrar(id):
    Article.query.filter_by(id=id).delete()
    return redirect(url_for('dashboard_articulos'))


@app.route("/categoria/borrar/<int:id>")
@login_required
def categoria_borrar(id):
    Category.query.filter_by(id=id).delete()
    return redirect(url_for('dashboard_categorias'))

@app.route("/comentario/borrar/<int:id>")
@login_required
def comentario_borrar(id):
    Comment.query.filter_by(id=id).delete()
    return redirect(url_for('dashboard_comentarios'))

@app.route("/logout")
@login_required
def logout():
    session.clear()
    return redirect(url_for('login'))

if __name__ == "__main__":
    app.run(threaded=True)